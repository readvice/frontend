# Vue.js client for readvice

## Screenshots

Question Page
![Question Page](https://gitlab.com/asangelov/readvice-frontend/-/raw/master/screenshots/question-page.png)

---

Sign Up Page
![Sign Up Page](https://gitlab.com/asangelov/readvice-frontend/-/raw/master/screenshots/register-page.png)

---

Adding a 2FA token
![TOTP page](https://gitlab.com/asangelov/readvice-frontend/-/raw/master/screenshots/totp-page.png)

---

Admin Dashboard
![Admin Dashboard](https://gitlab.com/asangelov/readvice-frontend/-/raw/master/screenshots/admin-dashboard.png)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
