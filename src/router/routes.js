import Users from '../components/UserList'
import QuestionList from '../components/QuestionList'
import QuestionItem from '../components/QuestionItem'
import QuestionEdit from '../components/QuestionEdit'
import SignIn from '../components/SignIn'
import AdminDashboard from '../components/AdminDashboard'
import Register from '../components/Register'
import store from '../store'
import {refreshToken} from '../store/utils'
import QuestionAdd from '../components/QuestionAdd'
import UserItem from '../components/UserItem'
import QuestionSingle from '../components/QuestionSingle'
import NotFound from '../components/NotFound'
import Settings from '../components/Settings/Settings'
import SearchResults from '../components/SearchResults'

export default [
  {
    path: '/',
    redirect: {name: 'questionList'},
  },
  {
    path: '/users',
    component: Users,
    name: "userList",
    meta: { requiresAuth: true, title: "Users" }
  },
  {
    path: '/users/:id',
    component: UserItem,
    name: "userItem",
    meta: { requiresAuth: true, title: "Profile" }
  },
  {
    path: '/questions',
    component: QuestionList,
    name: "questionList",
    meta: { title: "Questions" }
  },
  {
    path: '/questions/create',
    component: QuestionAdd,
    name: "questionAdd",
    meta: { requiresAuth: true, title: "Create Question" }
  },
  {
    path: '/questions/:id',
    component: QuestionSingle,
    name: "questionSingle",
    meta: { title: "View Question" }
  },
  {
    path: '/questions/:id/edit',
    component: QuestionEdit,
    name: "questionEdit",
    meta: { requiresAuth: true, title: "Edit Question" }
  },
  {
    path: '/admin-dashboard',
    component: AdminDashboard,
    name: 'adminDashboard',
    meta: { requiresAuth: true, title: "Admin Dashboard" }
  },
  {
    path: '/signin',
    component: SignIn,
    name: "signin",
    meta: { guest: true, title: "Sign In" }
  },
  {
    path: '/register',
    component: Register,
    name: "register",
    meta: { guest: true, title: "Register" }
  },
  {
    path: '/settings',
    component: Settings,
    name: "settings",
    meta: { requiresAuth: true, title: "Settings" }
  },
  {
    path: '/search-results/:query',
    component: SearchResults,
    name: "searchResults",
    meta: { title: "Search Results... " }
  },
  {
    path: '*',
    component: NotFound,
    meta: { title: "404 Not Found" }
  }
]
