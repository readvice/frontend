import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import store from '../store'
import {refreshToken} from '../store/utils'

Vue.use(VueRouter);

const router = new VueRouter({
  scrollBehavior(to) {
    if (to.hash) {
      return window.scrollTo({ top: document.querySelector(to.hash).offsetTop, behavior: 'smooth' });
    }
    return window.scrollTo({ top: 0, behavior: 'smooth' });
  },
  mode: 'history',
  routes,
});

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.getIsAuthenticated) {
      try {
        const statusCode = await refreshToken();
        if (statusCode !== 200) {
          next({name: 'signin', query: { from: to.path }});
        } else {
          next();
        }
      } catch (error) {
        next({name: 'signin', query: { from: to.path }});
      }
    }
    else {
      next()
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (store.getters.getIsAuthenticated) {
      next({name: 'questionList'});
      return;
    }
    next()
  } else {
    next()
  }
})

const APP_NAME = 'readvice';
router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = to.meta.title + " | " + APP_NAME || APP_NAME;
    if (store.getters.getErrorMessage) {
      store.dispatch('setError', "")
    }
    if (from.name == 'signin') {
      store.dispatch('setMFA', false)
    }
  });
});


export default router;
