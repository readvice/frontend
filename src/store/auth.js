import axios from 'axios'
import router from '../router'
import ENUM from './enums'
import {getAuthenticatedUser} from './utils'

const state = {
  isAuthenticated: false,
  user: "",
  intervalName: "",
  roles: [],
  tokens: [],
  MFA: false,
};

const getters = {
  getIsAuthenticated(state) {
    return state.isAuthenticated;
  },
  getCurrentUser(state) {
    return state.user;
  },
  getIntervalName(state) {
    return state.intervalName;
  },
  getRoles(state) {
    let roles = [];
    Object.values(state.roles).forEach(role => {
      roles.push(role.authority.substring(5, role.authority.length))
    })
    return roles;
  },
  getTokens(state) {
    return state.tokens;
  },
  getMFA(state) {
    return state.MFA;
  }
};

const mutations = {
  setIsAuthenticated(state, isAuthenticated) {
    state.isAuthenticated = isAuthenticated;
  },
  setCurrentUser(state, currentUser) {
    state.user = currentUser
  },
  setIntervalName(state, intervalName) {
    state.intervalName = intervalName;
  },
  setRoles(state, roles) {
    state.roles = roles
  },
  clearCurrentUser(state) {
    state.user = ""
  },
  setTokens(state, tokens) {
    state.tokens = tokens
  },
  setMFA(state, mfa) {
    state.MFA = mfa;
  }
};

const actions = {
  async login({ commit, dispatch }, payload) {
    commit('SET_API_STATE', ENUM.LOADING)
    axios.post("/api/login", payload.loginData)
      .then(async response => {
        commit('SET_API_STATE', ENUM.LOADED)
        commit('SET_ERROR_MESSAGE', "");
        commit('setMFA', false);
        if (response.data.status === "SUCCESS") {
          await getAuthenticatedUser()
          await router.replace(payload.from)
        } else if (response.data === "MFA") {
          commit('setMFA', true);
        }
      })
      .catch (err => {
        console.log(err)
        commit('SET_API_STATE', ENUM.ERROR)
        commit('SET_ERROR_MESSAGE', err.response.data.message);
      })

  },
  async loginStep2({ commit, dispatch }, payload) {
    commit('SET_API_STATE', ENUM.LOADING)
    axios.post('/api/verify?code=' + payload.code, payload.loginData)
      .then(async response => {
        commit('SET_API_STATE', ENUM.LOADED)
        commit('SET_ERROR_MESSAGE', "");
        if (response.data.status === "SUCCESS") {
          await getAuthenticatedUser()
          if (payload.from)
            await router.replace(payload.from)
          else
            await router.replace({name: 'questionList'})
        }
      })
      .catch(err => {
        console.log(err)
        commit('SET_API_STATE', ENUM.ERROR)
        if (!err.response) {
          commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
        }
        commit('SET_ERROR_MESSAGE', err.response.data.message);
      })
  },
  async register({ commit, dispatch }, payload) {
    commit('SET_API_STATE', ENUM.LOADING)
    axios.post('api/users', payload.loginData)
      .then(response => {
        commit('SET_API_STATE', ENUM.LOADED)
        dispatch('loadUsers')
        router.replace({name: 'signin', params: { from: payload.from }})
      })
      .catch(err => {
        commit('SET_API_STATE', ENUM.ERROR)
        if (!err.response) {
          commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
        }
        commit('SET_ERROR_MESSAGE', err.response.data.message);
      });
  },
  async getCurrentUser({ commit, dispatch }) {},
  async refresh({ commit, dispatch, getters }) {},
  async logout({ commit, dispatch, getters }) {},
  setMFA({ commit, dispatch }, mfa) {
    commit('setMFA', mfa)
  }
};

export default {
  namespaced: false,
  state,
  getters,
  mutations,
  actions
};
