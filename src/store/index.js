import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import axios from 'axios'
import ENUM from './enums'
import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apiState: ENUM.INIT,
    questions: [],
    users: [],
    errorMessage: "",
  },
  mutations: {
    LOAD_QUESTIONS(state, questions) {
      state.questions = questions;
      state.apiState = ENUM.LOADED;
    },
    LOAD_USERS(state, users) {
      state.users = users;
    },
    SET_API_STATE(state, apiState) {
      state.apiState = apiState;
    },
    ADD_QUESTION(state, question) {
      state.questions.push(question);
    },
    UPDATE_QUESTION(state, question) {
      let index = state.questions.findIndex(q => q.id === question.id)
      Vue.set(state.questions, index, question)
    },
    DELETE_QUESTION(state, question) {
      state.questions.splice(state.questions.indexOf(question), 1)
    },
    ADD_ANSWER(state, { answer, questionId }) {
      let q = state.questions.find(question => question.id == questionId)
      if (q.answers == null) {
        q.answers = []
      }
      q.answers.push(answer);
    },
    DELETE_ANSWER(state, { answer, questionId }) {
      let q = state.questions.find(question => question.id == questionId)
      let i = q.answers.map(item => item.id).indexOf(answer)
      q.answers.splice(i, 1)
    },
    DOWNVOTE_QUESTION(state, { questionId, userId }) {
      let q = state.questions.find(question => question.id == questionId)
      if (q.downvotedBy.includes(userId)) {
        q.downvotedBy.splice(q.downvotedBy.indexOf(userId))
      } else {
        q.downvotedBy.push(userId)
        q.upvotedBy.splice(q.upvotedBy.indexOf(userId))
      }
    },
    UPVOTE_QUESTION(state, { questionId, userId }) {
      let q = state.questions.find(question => question.id == questionId)
      if (q.upvotedBy.includes(userId)) {
        console.log('test')
        q.upvotedBy.splice(q.upvotedBy.indexOf(userId))
      } else {
        q.upvotedBy.push(userId)
        q.downvotedBy.splice(q.downvotedBy.indexOf(userId))
      }
    },
    SET_ERROR_MESSAGE(state, message) {
      state.errorMessage = message
    }
  },
  getters: {
    getQuestions(state) {
      return state.questions
    },
    getQuestionById: (state) => (id) => {
      return state.questions.find(q => q.id == id)
    },
    getUsers(state) {
      return state.users
    },
    getUserById: (state) => (id) => {
      return state.users.find(u => u.id == id)
    },
    getErrorMessage(state) {
      return state.errorMessage
    }
  },
  actions: {
    loadQuestions({ commit }) {
      commit('SET_API_STATE', ENUM.LOADING);
      axios.get("/api/questions")
        .then(response => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('LOAD_QUESTIONS', response.data)
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    loadUsers({ commit }) {
      commit('SET_API_STATE', ENUM.LOADING);
      axios.get("/api/users")
        .then(response => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('LOAD_USERS', response.data)
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    addQuestion({ commit }, question) {
      commit('SET_API_STATE', ENUM.LOADING);
      axios.post('api/questions', question)
        .then(response => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('ADD_QUESTION', response.data)
          router.push({name: 'questionSingle', params: {id: response.data.id}})
         })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    editQuestion({ commit }, payload) {
      commit('SET_API_STATE', ENUM.LOADING);
      axios.put('/api/questions/' + payload.id, payload.question)
        .then(response => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('UPDATE_QUESTION', response.data)
          router.push({ name: 'questionSingle', params: {id: response.data.id} });
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    deleteQuestion({ commit }, payload) {
      commit('SET_API_STATE', ENUM.LOADING);
      axios.delete('/api/questions/' + payload.id)
        .then(() => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('DELETE_QUESTION', payload)
          router.push({ name: 'questionList'});
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    downvoteQuestion({ commit }, payload) {
      axios.put('/api/questions/' + payload.id + '/downvote')
        .then(() => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('DOWNVOTE_QUESTION', {
            questionId: payload.id,
            userId: payload.userId,
          })
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          console.log(err)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    upvoteQuestion({ commit }, payload) {
      axios.put('/api/questions/' + payload.id + '/upvote')
        .then(() => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('UPVOTE_QUESTION', {
            questionId: payload.id,
            userId: payload.userId,
          })
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          console.log(err)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    addAnswer({ commit }, payload) {
      axios.post('/api/questions/' + payload.id + '/answers', payload.answer)
        .then(response => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('ADD_ANSWER', {answer: response.data, questionId: payload.id})
          router.push({ name: 'questionSingle', params: {id: payload.id }});
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    deleteAnswer({ commit }, payload) {
      axios.delete('/api/questions/' + payload.questionId + '/answers/' + payload.answerId)
        .then(() => {
          commit('SET_API_STATE', ENUM.LOADED);
          commit('DELETE_ANSWER', {answer: payload.answerId, questionId: payload.questionId})
        })
        .catch(err => {
          commit('SET_API_STATE', ENUM.ERROR)
          if (!err.response) {
            commit('SET_ERROR_MESSAGE', "You don't have an internet connection or the server is down."); return;
          }
          commit('SET_ERROR_MESSAGE', err.response.data.message);
        })
    },
    setError({ commit }, message) {
      commit('SET_ERROR_MESSAGE', message)
    }
  },
  modules: {
    auth
  }
})
