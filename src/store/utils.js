import store from "../store/index";
import axios from 'axios'
import router from '../router'

export async function Logout() {
  let response = await axios.get('api/logout')
  const intervalName = store.getters.getIntervalName;
  if (intervalName) clearInterval(intervalName);
  store.commit("setIsAuthenticated", false);
  store.commit("clearCurrentUser");
  router.replace({name: 'questionList'});
}

export async function refreshTokenInternal() {
  try {
    const response = await axios.post("/api/refresh");
    if (response.status !== 200) Logout();
  } catch (error) {
    Logout();
  }
}

export async function refreshToken() {
  const response = await axios.post("/api/refresh");
  return response.status;
}

export async function getAuthenticatedUser() {
  try {
    const response = await axios.get("/api/users/me");
    if (response.status === 200) {
      let currentUser = response.data
      store.commit("setCurrentUser", currentUser);
      store.commit("setRoles", currentUser.authorities.filter( el => el.authority.startsWith("ROLE_") ))
      store.commit("setIsAuthenticated", true);
      let tokens = await axios.get('/api/users/' + currentUser.principal.id + '/2fa');
      store.commit("setTokens", tokens.data);
      await refreshTokenInternal();
      const intervalName = setInterval(async () => {
        await refreshTokenInternal();
      }, intervalMilliSeconds);

      store.commit("setIntervalName", intervalName);
    } else {
      Logout();
    }
  } catch (error) {
  }
}

export const intervalMilliSeconds = 900000; // 15 minutes
