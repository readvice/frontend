// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
/* eslint-disable */
import Vue from 'vue'
import App from './App';
import store from './store'
import axios from 'axios'
import router from './router'
import {getAuthenticatedUser, refreshToken} from './store/utils'

require('@/store/subscriber')

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL;
axios.defaults.withCredentials = true;

Vue.config.productionTip = false

Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted: function (el) {
    // Focus the element
    el.focus()
  }
})

getAuthenticatedUser().then(() => {
  new Vue({
    router,
    store,
    render: h => h(App),
  }).$mount('#app');
})

